# Bot discord pour la gestion des événements

## Installation en local sur linux pour dev

Inspiré de cette page: https://discordpy.readthedocs.io/en/stable/intro.html

```
# On clone le projet
git clone https://gitlab.com/Kayo/discord-event
# On se met dedans
cd discord-event
# On crée l'environnement python
python3 -m venv bot-env
# On active l'environnement python
source bot-env/bin/activate
# On installe les dépendances du projet
python -m pip install -r requirements.txt
```

Il faut ensuite pour que cela fonctionne, créer un fichier `bot_token.py` et 
mettre la variable `TOKEN` avec le token du bot.

```
TOKEN = "LE_TOKEN"
```

Puis pour lancer le bot...
```
python3 test_bot.py
```
