#
#####
#
# Bot discord pour la gestion des événements !
# Ce bot permet de....
# - Créer un salon temporaire dédié en dessous du canal annonces
# - Mettre un message auto pour l'événement dans le canal annonce
# - Supprimer automatiquement le canal temporaire et le message dans annonce au bout de 7 jours
#
#####
#

import discord
from datetime import datetime
from bot_token import TOKEN

intents = discord.Intents.default()
intents.guild_scheduled_events = True

client = discord.Client(intents=intents)

###############################################
# Variables
###############################################

SERVER_ID = 1016733921913868308 
CHANNEL_ANNONCE_ID = 1016770621880946689

###############################################
# Fonctions
###############################################

@client.event
async def on_scheduled_event_create(event):
    # Crée un canal lié à l'événement
    guild = client.get_guild(SERVER_ID)
    channel_annonce = client.get_channel(CHANNEL_ANNONCE_ID)
    channel_new = await guild.create_text_channel("🐹-"+str(event.name), category=channel_annonce.category)

    # On edite la description de l'événement pour ajouter le nouveau canal
    await event.edit(location=event.location,end_time=event.end_time,description=str(event.description+"\nPour s'organiser -> "+channel_new.mention))
    new_channel_message = '------------------\n**' + event.name + '**' + '\n\n' + event.description + '\n\n*(le ' + event.start_time.strftime("%m/%d/%Y à %H:%M") + " jusqu'au " + event.end_time.strftime("%m/%d/%Y à %H:%M") + ')*\n------------------' 

    # Met un message dans un canal "annonce" et le nouveau canal
    if event.cover_image is not None:
        image_file = await event.cover_image.to_file()
        await channel_new.send(file=image_file)
    await channel_new.send(new_channel_message)
    await channel_annonce.send(new_channel_message)

@client.event
async def on_scheduled_event_update(event_before, event_after):
    # Quand l'événement est terminé, ou supprimé
    if event_after.status == discord.EventStatus.ended or event_after.status == discord.EventStatus.cancelled: 
        # On supprime le channel créé pour l'event
        channel_event_id = event_after.description.splitlines()[-1][22:41] # Récupère l'ID depuis la description
        channel_event = client.get_channel(int(channel_event_id))
        if channel_event is not None:
            await channel_event.delete()

        channel = client.get_channel(CHANNEL_ANNONCE_ID)
        await channel.send('Un event est terminé')

@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')

client.run(TOKEN)
